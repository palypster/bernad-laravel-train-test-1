<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\CloseThread;
use Carbon\Carbon;
use App\Thread;

class QueueCloseThreads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatchJobClsThread';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch job CloseThread for the queue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
     {
    //     $job = (new CloseThread())
    //     ->delay(Carbon::now()->addSeconds(15));
    //     dispatch($job);
    
        CloseThread::dispatch(new CloseThread())
        ->delay(now()->addSeconds(15));

    }
}
