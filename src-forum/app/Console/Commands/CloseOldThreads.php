<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Jobs\CloseThread;
use App\Thread;

class CloseOldThreads extends Command
{
    private $count_closed;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clsoldthreads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close all threads with last comment older than month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->counter = 0;
        $this->chunk_count = 200;

        $threads = DB::table('threads as thr')->select('thr.id')->join('replies as rep', 'rep.thread_id', '=', 'thr.id')
            ->where([['rep.created_at', '<', Carbon::now()->subDays(30)]])  //,['thr.closed','=','false']
            ->groupBy('thr.id')->get();
        
        $threads->chunk($this->chunk_count)->map(function($chunk) {
           CloseThread::dispatch($chunk); 
        });

    }
}
