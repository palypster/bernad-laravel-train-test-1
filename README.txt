**During implementation I had to ask:**
https://stackoverflow.com/questions/57933820/how-to-update-with-laravel-db-query-with-limit

**I used for seeding my factories and tinker:**

$threads = factory('App\Thread',50)->create();
$threads->each(function($thread){factory('App\Reply',100)->create(['thread_id' => $thread->id]);});

**Running the command that will close all threads with the replies older than month:**

php artisan clsoldthreads

**Running the command for dispatching job that runs command with close threads limitation (uncomment/comment some code in CloseOldThreads command):**

php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread
php artisan dispatchJobClsThread

**Running the queue worker that will process the dispatched jobs:**

php artisan queue:work


**Basic comments/reply from DBE(Brackets employee):**

- in order to avoid memory leaks during seeding with tinker it is better to use classical code in seeder, seeding and migrate
- to avoid dispatching jobs manually you can bind them with loop
- it is possible to dispatch jobs with time limitation in other better way to avoid paralell run of processes in queue